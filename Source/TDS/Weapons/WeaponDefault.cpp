#include "WeaponDefault.h"

#include <shldisp.h>

#include "DrawDebugHelpers.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Engine/StaticMeshActor.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Projectiles/TraceProjectileDefault.h"

AWeaponDefault::AWeaponDefault()
{
    PrimaryActorTick.bCanEverTick = true;

    SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
    RootComponent = SceneComponent;

    SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
    SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
    SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
    SkeletalMeshWeapon->SetupAttachment(RootComponent);

    StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
    StaticMeshWeapon->SetGenerateOverlapEvents(false);
    StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
    StaticMeshWeapon->SetupAttachment(RootComponent);

    ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
    ShootLocation->SetupAttachment(RootComponent);

    MagazineDropLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("MagazineDropLocation"));
    MagazineDropLocation->SetupAttachment(RootComponent);

    ShellDropLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShellDropLocation"));
    ShellDropLocation->SetupAttachment(RootComponent);
}

void AWeaponDefault::WeaponInit(FWeaponParams InitWeaponParams)
{
    WeaponParams = InitWeaponParams;

    if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
    {
        SkeletalMeshWeapon->DestroyComponent(true);
    }

    if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
    {
        StaticMeshWeapon->DestroyComponent();
    }

    RoundLeft = WeaponParams.NumberRound;

    UpdateDispersionState(EMovementState::Run_State);
}

void AWeaponDefault::UpdateDispersionState(EMovementState NewMovementState)
{
    bIsBlockFire = false;

    switch (NewMovementState)
    {
    case EMovementState::Aim_State:
        CurrentDispersion = WeaponParams.DispersionWeapon.AimDispersion;
        break;
    case EMovementState::AimWalk_State:
        CurrentDispersion = WeaponParams.DispersionWeapon.AimWalkDispersion;
        break;
    case EMovementState::Walk_State:
        CurrentDispersion = WeaponParams.DispersionWeapon.WalkDispersion;
        break;
    case EMovementState::Run_State:
        CurrentDispersion = WeaponParams.DispersionWeapon.RunDispersion;
        break;
    case EMovementState::SprintRun_State:
        bIsBlockFire = true;
        SetFireState(false);
        break;
    default:
        break;
    }
}

void AWeaponDefault::SetFireState(bool bIsFire)
{
    if (GetBlockFireState())
    {
        bIsFiring = bIsFire;
    }
    else
    {
        bIsFiring = false;
        FireTimer = 0.01f;
    }
}

bool AWeaponDefault::GetBlockFireState()
{
    return !bIsBlockFire;
}

FName AWeaponDefault::GetAmmoType()
{
    return WeaponParams.AmmoType;
}

void AWeaponDefault::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    FireTick(DeltaTime);
    ReloadTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
    if (GetNumberRoundLeft() > 0)
    {
        if (bIsFiring)
        {
            if (FireTimer <= 0.f)
            {
                if (!bIsReloading)
                {
                    Fire();
                }
            }
        }

        if (FireTimer > 0.f)
        {
            FireTimer -= DeltaTime;
        }
    }
    else
    {
        OnWeaponEmptyAmmo.Broadcast();
    }
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
    if (bIsReloading)
    {
        if (ReloadTimer < 0.0f)
        {
            FinishReload();
        }
        else
        {
            ReloadTimer -= DeltaTime;
        }
    }
}

void AWeaponDefault::Fire()
{
    FireTimer = PlayAnimMontage(WeaponParams.AnimWeaponFire, false, WeaponParams.FireRate);
    RoundLeft = RoundLeft - 1;
    OnWeaponClipChanged.Broadcast(RoundLeft);

    UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponParams.SoundFireWeapon, ShootLocation->GetComponentLocation());
    UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponParams.EffectFireWeapon, ShootLocation->GetComponentTransform());

    const int8 NumberProjectile = GetNumberProjectileByShot();

    if (ShootLocation)
    {
        const FVector SpawnLocation = ShootLocation->GetComponentLocation();
        FRotator SpawnRotation = ShootLocation->GetComponentRotation();

        for (int8 i = 0; i < NumberProjectile; i++)
        {
            SpawnRotation = FMath::VRandCone(SpawnRotation.Vector(), FMath::DegreesToRadians(GetCurrentDispersion())).Rotation();

            FActorSpawnParameters SpawnParams;
            SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
            SpawnParams.Owner = GetOwner();
            SpawnParams.Instigator = GetInstigator();

            const FProjectileParams ProjectileParams = WeaponParams.ProjectileParams;

            if (ProjectileParams.ProjectileType == EProjectileType::Projectile)
            {
                AProjectileDefault* Projectile = GetWorld()->SpawnActor<AProjectileDefault>(ProjectileParams.ProjectileClass, SpawnLocation, SpawnRotation, SpawnParams);
                Projectile->InitProjectile(ProjectileParams);
            }
            else if (ProjectileParams.ProjectileType == EProjectileType::Trace)
            {
                ATraceProjectileDefault* Projectile = GetWorld()->SpawnActor<ATraceProjectileDefault>(SpawnLocation, SpawnRotation, SpawnParams);
                Projectile->InitProjectile(ProjectileParams);
            }

            PlayAnimMontage(GetFireAnimation(), false, GetFireRate());

            if (ShowDebug)
            {
                DrawDebugCone(GetWorld(), SpawnLocation, SpawnRotation.Vector(),
                              WeaponParams.ProjectileParams.LengthTrace,
                              FMath::DegreesToRadians(GetCurrentDispersion()),
                              FMath::DegreesToRadians(GetCurrentDispersion()),
                              32, FColor::Emerald, false, 1.f,
                              (uint8)'\000', 1.0f);
            }
        }

        if(WeaponParams.ShellDrop.Mesh)
        {
            DropMesh(ShellDropLocation, WeaponParams.ShellDrop);
        }
    }

    if(WeaponParams.AnimCharFire || WeaponParams.AnimCharAimFire)
    {
        OnWeaponFire.Broadcast(WeaponParams.AnimCharFire, WeaponParams.AnimCharAimFire);
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("Without fire animation"));
    }
}

float AWeaponDefault::GetCurrentDispersion()
{
    return CurrentDispersion;
}

int32 AWeaponDefault::GetNumberProjectileByShot()
{
    return WeaponParams.NumberProjectileByShot;
}

int32 AWeaponDefault::GetNumberRoundLeft()
{
    return RoundLeft;
}

float AWeaponDefault::GetReloadRate()
{
    return WeaponParams.ReloadRate;
}

float AWeaponDefault::GetFireRate()
{
    return WeaponParams.FireRate;
}

float AWeaponDefault::PlayAnimMontage(UAnimMontage* AnimMontage, const bool IsLooping, const float RateScale)
{
    float Duration = 0.f;

    if (AnimMontage)
    {
        AnimMontage->RateScale = RateScale;
        Duration = AnimMontage->CalculateSequenceLength() / RateScale;
        SkeletalMeshWeapon->PlayAnimation(AnimMontage, IsLooping);
    }

    return Duration;
}

UAnimMontage* AWeaponDefault::GetFireAnimation()
{
    return WeaponParams.AnimWeaponFire;
}

void AWeaponDefault::Reload(int AmmoCount)
{
    bIsReloading = true;
    AmmoToLoad = AmmoCount;
    ReloadTimer = PlayAnimMontage(WeaponParams.AnimWeaponReload, false, WeaponParams.ReloadRate);

    if(WeaponParams.AnimCharReload)
    {
        OnWeaponReloadStart.Broadcast(WeaponParams.AnimCharReload);
    }
}

void AWeaponDefault::DropMesh(const UArrowComponent* SpawnLocation, FSpawnStaticMeshParam SpawnStaticMeshParam)
{
    FActorSpawnParameters ActorSpawnParameters = FActorSpawnParameters();
    ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

    if (AStaticMeshActor* StaticMeshActor = GetWorld()->SpawnActor<AStaticMeshActor>(SpawnLocation->GetComponentLocation(), SpawnLocation->GetComponentRotation(), ActorSpawnParameters))
    {
        StaticMeshActor->SetLifeSpan(SpawnStaticMeshParam.LifeTime);

        if (UStaticMeshComponent* StaticMeshComponent = StaticMeshActor->GetStaticMeshComponent())
        {
            StaticMeshComponent->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
            StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
            StaticMeshComponent->Mobility = EComponentMobility::Movable;
            StaticMeshComponent->SetSimulatePhysics(true);
            StaticMeshComponent->SetStaticMesh(SpawnStaticMeshParam.Mesh);
            StaticMeshComponent->SetWorldScale3D(FVector(SpawnStaticMeshParam.Scale, SpawnStaticMeshParam.Scale, SpawnStaticMeshParam.Scale));
            FVector Direction = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(
                SpawnLocation->GetForwardVector().GetSafeNormal() , 30
            );
            StaticMeshComponent->AddImpulse(Direction * SpawnStaticMeshParam.Impulse);
        }
    }
}

int32 AWeaponDefault::GetClipSize()
{
    return WeaponParams.NumberRound;
}

void AWeaponDefault::FinishReload()
{
    bIsReloading = false;
    RoundLeft += AmmoToLoad;
    OnWeaponClipChanged.Broadcast(RoundLeft);
    OnWeaponReloadEnd.Broadcast(GetAmmoType(), AmmoToLoad);

    if(WeaponParams.MagazineDrop.Mesh)
    {
        DropMesh(MagazineDropLocation, WeaponParams.MagazineDrop);
    }
}
