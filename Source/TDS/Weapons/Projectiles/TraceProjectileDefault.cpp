#include "TraceProjectileDefault.h"
#include "DrawDebugHelpers.h"

ATraceProjectileDefault::ATraceProjectileDefault()
{
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;
}

void ATraceProjectileDefault::InitProjectile(const FProjectileParams& InitProjectileParams)
{
	Super::InitProjectile(InitProjectileParams);
	
	FCollisionQueryParams CollisionQueryParams = FCollisionQueryParams();
	CollisionQueryParams.bReturnPhysicalMaterial = true;

	FHitResult HitResult;
	FVector LineEnd = GetActorLocation() + GetActorForwardVector() * ProjectileParams.LengthTrace;

	if (ShowDebug)
	{
		DrawDebugLine(GetWorld(), GetActorLocation(), LineEnd, FColor::Red, false, 5.f);
	}
	
	if (GetWorld()->LineTraceSingleByChannel(HitResult, GetActorLocation(), LineEnd, ECC_WorldDynamic, CollisionQueryParams))
	{
		Hit(HitResult);
	}

	Destroy();
}
