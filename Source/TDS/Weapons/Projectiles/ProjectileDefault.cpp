#include "ProjectileDefault.h"
#include "DrawDebugHelpers.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

AProjectileDefault::AProjectileDefault()
{
    BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
    BulletCollisionSphere->SetSphereRadius(16.f);
    BulletCollisionSphere->bReturnMaterialOnMove = true;
    BulletCollisionSphere->SetCanEverAffectNavigation(false);
    BulletCollisionSphere->SetNotifyRigidBodyCollision(true);
    BulletCollisionSphere->SetCollisionObjectType(ECC_EngineTraceChannel2);
    BulletCollisionSphere->SetCollisionProfileName(FName("Projectile"));

    RootComponent = BulletCollisionSphere;

    BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
    BulletProjectileMovement->UpdatedComponent = BulletCollisionSphere;
    BulletProjectileMovement->MaxSpeed = 5000.f;
    BulletProjectileMovement->bRotationFollowsVelocity = true;
}

void AProjectileDefault::InitProjectile(const FProjectileParams& InitProjectileParams)
{
    Super::InitProjectile(InitProjectileParams);

    SetLifeSpan(ProjectileParams.LifeTime);
    BulletProjectileMovement->Velocity = BulletProjectileMovement->Velocity.GetSafeNormal() * InitProjectileParams.InitSpeed;
}
