#pragma once

#include "CoreMinimal.h"
#include "ProjectileBase.h"

#include "TDS/FuncLibrary/Types.h"
#include "TraceProjectileDefault.generated.h"

UCLASS()
class TDS_API ATraceProjectileDefault : public AProjectileBase
{
	GENERATED_BODY()
public:
	ATraceProjectileDefault();
	
	virtual void InitProjectile(const FProjectileParams& InitProjectileParams) override;
};
