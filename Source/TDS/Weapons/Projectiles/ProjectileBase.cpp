#include "ProjectileBase.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/KismetMathLibrary.h"

AProjectileBase::AProjectileBase()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AProjectileBase::Hit(const FHitResult& HitResult)
{
	AActor* HitActor = HitResult.GetActor();

	if (HitActor)
	{
		if (ProjectileParams.DamageRadius > 0.f)
		{
			ApplyDamageOnRadius(HitResult.Location, ProjectileParams.DamageRadius, ProjectileParams.Damage, ProjectileParams.DamageRadiusFalloffCurve);
		}
		else
		{
			UGameplayStatics::ApplyDamage(HitActor, ProjectileParams.Damage, GetInstigatorController(), this, nullptr);
		}

		if(HitResult.PhysMaterial.IsValid())
		{
			const EPhysicalSurface HitSurfaceType = UGameplayStatics::GetSurfaceType(HitResult);
			UMaterialInterface* HitMaterial;

			if (ProjectileParams.HitDecals.Contains(HitSurfaceType))
			{
				HitMaterial = ProjectileParams.HitDecals[HitSurfaceType];
			}
			else
			{
				HitMaterial = ProjectileParams.HitDecals[SurfaceType_Default];
			}

			if (HitMaterial)
			{
				if(HitResult.GetComponent())
				{
					FRotator Rotator = HitResult.ImpactNormal.Rotation();
					Rotator = FRotator(Rotator.Pitch, Rotator.Yaw, Rotator.Roll + UKismetMathLibrary::RandomFloat() * 360);
					UGameplayStatics::SpawnDecalAttached(HitMaterial, FVector(20.0f), HitResult.GetComponent(),
					                                     NAME_None, HitResult.ImpactPoint, Rotator,
					                                     EAttachLocation::KeepWorldPosition, ProjectileParams.LifeTime);
				}
			}

			if (ProjectileParams.HitFXs.Contains(HitSurfaceType))
			{
				UParticleSystem* HitParticle = ProjectileParams.HitFXs[HitSurfaceType];

				if (HitParticle)
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitParticle, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
				}
			}

			if (ProjectileParams.HitSound)
			{
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileParams.HitSound, HitResult.ImpactPoint);
			}
		}
	}

	if (ProjectileParams.ExplosionFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileParams.ExplosionFX, HitResult.Location);
	}
}

void AProjectileBase::ApplyDamageOnRadius(const FVector Location, const float Radius, const float Damage, const UCurveFloat* DamageFalloffCurve)
{
	TArray<AActor*> HitActors;
	const bool bHit = UKismetSystemLibrary::SphereOverlapActors(GetWorld(), Location, Radius, TArray<TEnumAsByte<EObjectTypeQuery>>(), nullptr,  TArray<AActor*>(), HitActors);

	if (bHit)
	{
		for (AActor* HitActor : HitActors)
		{
			float CurrentDamage = Damage;

			if (DamageFalloffCurve)
			{
				const float DamageFactor = DamageFalloffCurve->GetFloatValue(UKismetMathLibrary::Vector_Distance(Location, HitActor->GetActorLocation()) / Radius);
				CurrentDamage = CurrentDamage * DamageFactor;
			}

			UGameplayStatics::ApplyDamage(HitActor, CurrentDamage, GetInstigatorController(), this, nullptr);
		}

		if (ShowDebug)
		{
			if (DamageFalloffCurve)
			{
				TArray<FRichCurveKey> CurveKeys = DamageFalloffCurve->FloatCurve.Keys;
				for (const FRichCurveKey CurveKey : CurveKeys)
				{
					if (!UKismetMathLibrary::NearlyEqual_FloatFloat(CurveKey.Value, 1.f))
					{
						UE_LOG(LogTemp, Log, TEXT("Falloff key: %f"), CurveKey.Value);
						DrawDebugSphere(GetWorld(), Location, ProjectileParams.DamageRadius * CurveKey.Value, 8, FColor::Yellow, false, 5.f);
					}
				}
			}
			else
			{
				DrawDebugSphere(GetWorld(), Location, ProjectileParams.DamageRadius, 8, FColor::Yellow, false, 5.f);
			}
		}
	}
}

void AProjectileBase::InitProjectile(const FProjectileParams& InitProjectileParams)
{
	ProjectileParams = InitProjectileParams;
}
