// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"

bool UTDSGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponParams& OutInfo)
{
	bool bIsFind = false;

	if (WeaponInfoTable)
	{
		FWeaponParams* WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponParams>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));
	}

	return bIsFind;
}

bool UTDSGameInstance::GetAmmoInfoByName(FName NameAmmo, FAmmoParams& OutInfo)
{
	bool bIsFind = false;

	if (AmmoInfoTable)
	{
		FAmmoParams* AmmoInfoRow = AmmoInfoTable->FindRow<FAmmoParams>(NameAmmo, "", false);
		if (AmmoInfoRow)
		{
			bIsFind = true;
			OutInfo = *AmmoInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSGameInstance::GetAmmoInfoByName - AmmoTable -NULL"));
	}

	return bIsFind;
}
