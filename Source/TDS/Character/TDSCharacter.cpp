// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TDS/HealthComponent.h"
#include "TDS/Game/TDSGameInstance.h"
#include "TDS/Inventory/InventoryComponent.h"
#include "TDS/Inventory/PickupableItem.h"

ATDSCharacter::ATDSCharacter()
{
    // Set size for player capsule
    GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

    // Don't rotate character to camera direction
    bUseControllerRotationPitch = false;
    bUseControllerRotationYaw = false;
    bUseControllerRotationRoll = false;

    // Configure character movement
    GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
    GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
    GetCharacterMovement()->bConstrainToPlane = true;
    GetCharacterMovement()->bSnapToPlaneAtStart = true;

    // Create a camera boom...
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(RootComponent);
    CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
    CameraBoom->TargetArmLength = TargetZoomLength;
    CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
    CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

    // Listener position
    ListenerLocation = CreateDefaultSubobject<USceneComponent>(TEXT("ListenerPosition"));
    ListenerLocation->SetupAttachment(RootComponent);
    ListenerLocation->SetRelativeLocation(FVector(0.f, 0.f, 80.f));

    // Create a camera...
    TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
    TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

    InventoryComponent = CreateDefaultSubobject<UInventoryComponent>(TEXT("InventoryComponent"));
    HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComponent"));

    // Activate ticking in order to update the cursor every frame.
    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

    if (CurrentCursor)
    {
        APlayerController* myPC = Cast<APlayerController>(GetController());

        if (myPC)
        {
            FHitResult TraceHitResult;
            myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);

            CurrentCursor->SetWorldLocation(TraceHitResult.Location);
        }
    }

    MovementTick(DeltaSeconds);
}

void ATDSCharacter::BeginPlay()
{
    APlayerController* mainController = UGameplayStatics::GetPlayerController(UObject::GetWorld(), 0);
    mainController->SetAudioListenerOverride(ListenerLocation, FVector::ZeroVector, FRotator::ZeroRotator);

    GameInstance = Cast<UTDSGameInstance>(GetGameInstance());

    FWeaponParams InitWeaponInfo;
    if (GameInstance->GetWeaponInfoByName(InitWeaponName, InitWeaponInfo))
    {
        if (InventoryComponent->AddWeapon(InitWeaponName, InitWeaponInfo.NumberRound))
        {
            AddWeapon(InitWeaponName, InitWeaponInfo.NumberRound);
        }
    }

    if (CursorMaterial)
    {
        CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
    }

    HealthComponent->OnDead.AddDynamic(this, &ATDSCharacter::Die);

    Super::BeginPlay();
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
    PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);
    PlayerInputComponent->BindAxis(TEXT("Zoom"), this, &ATDSCharacter::CameraZoom);

    PlayerInputComponent->BindAction<FInputStateDelegate>(TEXT("Aim"), EInputEvent::IE_Pressed, this, &ATDSCharacter::ChangeMovementMode, EMovementState::Aim_State, true);
    PlayerInputComponent->BindAction<FInputStateDelegate>(TEXT("SlowWalk"), EInputEvent::IE_Pressed, this, &ATDSCharacter::ChangeMovementMode, EMovementState::Walk_State, true);
    PlayerInputComponent->BindAction<FInputStateDelegate>(TEXT("Sprint"), EInputEvent::IE_Pressed, this, &ATDSCharacter::ChangeMovementMode, EMovementState::Run_State, true);
    PlayerInputComponent->BindAction<FInputStateDelegate>(TEXT("Aim"), EInputEvent::IE_Released, this, &ATDSCharacter::ChangeMovementMode, EMovementState::Aim_State, false);
    PlayerInputComponent->BindAction<FInputStateDelegate>(TEXT("SlowWalk"), EInputEvent::IE_Released, this, &ATDSCharacter::ChangeMovementMode, EMovementState::Walk_State, false);
    PlayerInputComponent->BindAction<FInputStateDelegate>(TEXT("Sprint"), EInputEvent::IE_Released, this, &ATDSCharacter::ChangeMovementMode, EMovementState::Run_State, false);

    PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InputAttackPressed);
    PlayerInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::InputAttackReleased);
    PlayerInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDSCharacter::TryReloadWeapon);

    PlayerInputComponent->BindAction<FSelectedWeaponDelegate>(TEXT("WeaponOne"), EInputEvent::IE_Pressed, this, &ATDSCharacter::SelectWeapon, 0);
    PlayerInputComponent->BindAction<FSelectedWeaponDelegate>(TEXT("WeaponTwo"), EInputEvent::IE_Pressed, this, &ATDSCharacter::SelectWeapon, 1);
    PlayerInputComponent->BindAction<FSelectedWeaponDelegate>(TEXT("WeaponThree"), EInputEvent::IE_Pressed, this, &ATDSCharacter::SelectWeapon, 2);
    PlayerInputComponent->BindAction<FSelectedWeaponDelegate>(TEXT("WeaponFour"), EInputEvent::IE_Pressed, this, &ATDSCharacter::SelectWeapon, 3);
    PlayerInputComponent->BindAction<FSelectedWeaponDelegate>(TEXT("WeaponFive"), EInputEvent::IE_Pressed, this, &ATDSCharacter::SelectWeapon, 4);

    PlayerInputComponent->BindAction(TEXT("WeaponDrop"), EInputEvent::IE_Pressed, this, &ATDSCharacter::WeaponDrop);
}

void ATDSCharacter::InputAxisY(float Value)
{
    AxisY = Value;
}

void ATDSCharacter::InputAxisX(float Value)
{
    AxisX = Value;
}

void ATDSCharacter::InputAttackPressed()
{
    AttackCharEvent(true);
}

void ATDSCharacter::InputAttackReleased()
{
    AttackCharEvent(false);
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
    if (!bIsAlive)
        return;

    AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
    AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

    if (MovementState == EMovementState::SprintRun_State &&
        (AxisX == 1 || AxisX == -1 || AxisY == 1 || AxisY == -1))
    {
        const FVector TargetDirection = FVector(AxisX,AxisY,0.0f);
        const FRotator TargetRotation = TargetDirection.ToOrientationRotator();
        const FVector Interpolation = FMath::VInterpTo( GetActorRotation().Vector(), TargetRotation.Vector(), GetWorld()->GetDeltaSeconds(), 1);
        SetActorRotation(Interpolation.Rotation());
    }
    else
    {
        APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
        if (myController)
        {
            FHitResult ResultHit;
            myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

            SpineAngle = -UKismetMathLibrary::FindLookAtRotation(CurrentWeapon->ShootLocation->GetComponentLocation(), ResultHit.Location).Pitch;

            if (SpineAngle > SpineAngleMaxLimit)
                SpineAngle = SpineAngleMaxLimit;

            const float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
            SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));
        }
    }
}

float ATDSCharacter::GetSpineAngle()
{
    return SpineAngle;
}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
    if (CurrentWeapon)
    {
        CurrentWeapon->SetFireState(bIsFiring);
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
    }
}

void ATDSCharacter::CharacterUpdate()
{
    float ResSpeed = 600.0f;
    switch (MovementState)
    {
        case EMovementState::Aim_State:
            ResSpeed = MovementSpeedInfo.AimSpeedNormal;
            break;
        case EMovementState::AimWalk_State:
            ResSpeed = MovementSpeedInfo.AimSpeedWalk;
            break;
        case EMovementState::Walk_State:
            ResSpeed = MovementSpeedInfo.WalkSpeedNormal;
            break;
        case EMovementState::Run_State:
            ResSpeed = MovementSpeedInfo.RunSpeedNormal;
            break;
        case EMovementState::SprintRun_State:
            ResSpeed = MovementSpeedInfo.SprintRunSpeedRun;
            break;
        default:
            break;
    }

    GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
    if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
    {
        MovementState = EMovementState::Run_State;
    }
    else
    {
        if (SprintRunEnabled)
        {
            WalkEnabled = false;
            AimEnabled = false;
            MovementState = EMovementState::SprintRun_State;
        }
        if (WalkEnabled && !SprintRunEnabled && AimEnabled)
        {
            MovementState = EMovementState::AimWalk_State;
        }
        else
        {
            if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
            {
                MovementState = EMovementState::Walk_State;
            }
            else
            {
                if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
                {
                    MovementState = EMovementState::Aim_State;
                }
            }
        }
    }
    CharacterUpdate();

    //Weapon state update
    if (CurrentWeapon)
    {
        CurrentWeapon->UpdateDispersionState(MovementState);
    }
}

void ATDSCharacter::Die()
{
    bIsAlive = false;
    bIsReloading = false;
    AttackCharEvent(false);
    CurrentCursor->SetVisibility(false);
    UnPossessed();

    if (auto anim_instance = GetMesh()->GetAnimInstance())
    {
        anim_instance->StopAllMontages(0.1f);
    }

    auto anim = DeathAnimations[FMath::RandHelper(DeathAnimations.Num())];
    PlayAnimMontage(anim);
    GetWorldTimerManager().SetTimer(DeathAnimTimer, this, &ATDSCharacter::SetRagdoll, anim->GetPlayLength(), false);
}

void ATDSCharacter::SetRagdoll()
{
    if (GetMesh())
    {
        GetMesh()->SetAnimInstanceClass(nullptr);
        GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
        GetMesh()->SetSimulatePhysics(true);
    }
}

void ATDSCharacter::ChangeMovementMode(EMovementState NewMovementState, bool State)
{
    switch (NewMovementState)
    {
        case EMovementState::Aim_State:
            AimEnabled = State;
            break;
        case EMovementState::Walk_State:
            WalkEnabled = State;
            break;
        case EMovementState::Run_State:
            SprintRunEnabled = State;
            break;
        default: ;
    }

    ChangeMovementState();
}

void ATDSCharacter::AddWeapon(FName Name, int AmmoInClip)
{
    FWeaponParams InitWeaponInfo;
    if (GameInstance->GetWeaponInfoByName(Name, InitWeaponInfo))
    {
        if (InitWeaponInfo.WeaponClass)
        {
            FVector SpawnLocation = FVector(0);
            FRotator SpawnRotation = FRotator(0);

            if (CurrentWeapon)
            {
                InventoryComponent->SetAmmoInClip(PrevWeaponSlot, CurrentWeapon->RoundLeft);
                CurrentWeapon->DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepRelative, false));
                CurrentWeapon->Destroy();
            }

            FActorSpawnParameters SpawnParams;
            SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
            SpawnParams.Owner = GetOwner();
            SpawnParams.Instigator = GetInstigator();

            AWeaponDefault* InitWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(InitWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
            if (InitWeapon)
            {
                FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
                InitWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
                CurrentWeapon = InitWeapon;
                InitWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDSCharacter::WeaponReloadStart);
                InitWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDSCharacter::WeaponReloadEnd);
                InitWeapon->OnWeaponFire.AddDynamic(this, &ATDSCharacter::WeaponFire);
                InitWeapon->OnWeaponEmptyAmmo.AddDynamic(this, &ATDSCharacter::TryReloadWeapon);
                InitWeapon->WeaponInit(InitWeaponInfo);
                InitWeapon->UpdateDispersionState(MovementState);
                InitWeapon->RoundLeft = AmmoInClip;
                OnWeaponChanged.Broadcast(Name, AmmoInClip, CurrWeaponSlot);
            }
        }
    }
    else
    {
        UE_LOG(LogTemp, Warning, TEXT("ATDSCharacter::InitWeapon - Weapon not found in table -NULL"));
    }
}

void ATDSCharacter::SelectWeapon(int Slot)
{
    if (InventoryComponent->IsValidWeaponSlot(Slot) && Slot != CurrWeaponSlot)
    {
        bIsReloading = false;
        PrevWeaponSlot = CurrWeaponSlot;
        CurrWeaponSlot = Slot;
        FWeaponSlot WeaponSlot = InventoryComponent->GetWeapon(Slot);
        AddWeapon(WeaponSlot.Name, WeaponSlot.AmmoInClip);
    }
}

void ATDSCharacter::TryReloadWeapon()
{
    if (CurrentWeapon)
    {
        if (CurrentWeapon->GetNumberRoundLeft() <= CurrentWeapon->WeaponParams.NumberRound && !bIsReloading)
        {
            int ammo_count = InventoryComponent->GetAvailableAmmo(CurrentWeapon->GetAmmoType(), CurrentWeapon->WeaponParams.NumberRound - CurrentWeapon->RoundLeft);
            if (ammo_count > 0)
            {
                CurrentWeapon->Reload(ammo_count);
            }
        }
    }
}

void ATDSCharacter::WeaponReloadStart(UAnimMontage* CharAnim)
{
    bIsReloading = true;
    PlayAnimMontage(CharAnim, GetCurrentWeapon()->GetReloadRate());
}

AWeaponDefault* ATDSCharacter::GetCurrentWeapon()
{
    return CurrentWeapon;
}

void ATDSCharacter::WeaponFire(UAnimMontage* FireAnim, UAnimMontage* FireAimAnim)
{
    UAnimMontage* Anim;

    if (AimEnabled)
    {
        Anim = FireAimAnim;
    }
    else
    {
        Anim = FireAnim;
    }

    PlayAnimMontage(Anim, GetCurrentWeapon()->GetFireRate());
}

void ATDSCharacter::WeaponReloadEnd(FName AmmoName,int AmmoLoaded)
{
    bIsReloading = false;

    FAmmoParams InitAmmoInfo;
    if (GameInstance->GetAmmoInfoByName(AmmoName, InitAmmoInfo))
    {
        InventoryComponent->AddAmmo(AmmoName, -AmmoLoaded, InitAmmoInfo.MaxCount);
    }
}

bool ATDSCharacter::GetReloadState()
{
    return bIsReloading;
}

void ATDSCharacter::WeaponDrop()
{
    int AvailableWeapon = InventoryComponent->GetAvailableWeaponForDrop(CurrWeaponSlot);
    if (AvailableWeapon >= 0 && InventoryComponent->CountWeapons() > 1)
    {
        int SlotToDrop = CurrWeaponSlot;
        SelectWeapon(AvailableWeapon);
        FWeaponSlot DroppedWeaponSlot;
        if (InventoryComponent->DropWeapon(SlotToDrop, DroppedWeaponSlot))
        {
            APickupableItem* DroppedWeapon = GetWorld()->SpawnActorDeferred<APickupableItem>(APickupableItem::StaticClass(), GetTransform());
            DroppedWeapon->Name = DroppedWeaponSlot.Name;
            DroppedWeapon->Type = EItemType::Weapon;
            DroppedWeapon->bIsNowDropped = true;
            UGameplayStatics::FinishSpawningActor(DroppedWeapon, GetTransform());
        }
    }
}

UDecalComponent* ATDSCharacter::GetCursorToWorld()
{
    return CurrentCursor;
}

float ATDSCharacter::GetSmoothTargetArmLength(const float AxisValue, float& TargetZoom, float CurrentZoom)
{
    const float destinationTarget = (-StepZoomDistance * AxisValue) + TargetZoom;

    TargetZoom = FMath::Clamp(destinationTarget, MinZoomDistance, MaxZoomDistance);

    return FMath::FInterpTo(CurrentZoom, TargetZoom, GetWorld()->DeltaTimeSeconds, 10);
}

void ATDSCharacter::CameraZoom(float Value)
{
    CameraBoom->TargetArmLength = GetSmoothTargetArmLength(Value, TargetZoomLength, CameraBoom->TargetArmLength);
}
