// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDS/Game/TDSGameInstance.h"
#include "TDS/Weapons/WeaponDefault.h"
#include "TDSCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnWeaponChanged, const FName, Name, const int, AmmoInClip, const int, Slot);

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
    GENERATED_BODY()
protected:
    virtual void BeginPlay() override;

public:
    ATDSCharacter();

    // Called every frame.
    virtual void Tick(float DeltaSeconds) override;

    virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

    /** Returns TopDownCameraComponent subobject **/
    FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
    /** Returns CameraBoom subobject **/
    FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UInventoryComponent* InventoryComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UHealthComponent* HealthComponent;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    bool bSelectWeaponOnPickup = true;

    UPROPERTY()
    UTDSGameInstance* GameInstance;

private:
    /** Top down camera */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* TopDownCameraComponent;

    /** Camera boom positioning the camera above the character */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent* CameraBoom;

    /** Listener location. */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class USceneComponent* ListenerLocation;

public:
    //Cursor
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
    UMaterialInterface* CursorMaterial = nullptr;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
    FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

    //Movement
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    EMovementState MovementState = EMovementState::Run_State;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    FCharacterSpeed MovementSpeedInfo;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    bool SprintRunEnabled = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    bool WalkEnabled = false;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
    bool AimEnabled = false;

    UPROPERTY(VisibleAnywhere, BlueprintAssignable)
    FOnWeaponChanged OnWeaponChanged;

    UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
    bool bIsAlive = true;

    UPROPERTY()
    UDecalComponent* CurrentCursor = nullptr;

    //Anims
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animations")
    TArray<UAnimMontage*> DeathAnimations;

    UPROPERTY()
    FTimerHandle DeathAnimTimer;

    //Weapon
    UPROPERTY()
    AWeaponDefault* CurrentWeapon = nullptr;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
    FName InitWeaponName;

    //Inputs
    UFUNCTION()
    void InputAxisY(float Value);
    UFUNCTION()
    void InputAxisX(float Value);
    UFUNCTION()
    void InputAttackPressed();
    UFUNCTION()
    void InputAttackReleased();

    float AxisX = 0.0f;
    float AxisY = 0.0f;
    float MinZoomDistance = 500.f;
    float MaxZoomDistance = 1500.f;
    float StepZoomDistance = 100.f;
    float TargetZoomLength = 800.f;
    float SpineAngle = 0.f;
    float SpineAngleMaxLimit = 50.f;

    int PrevWeaponSlot = 0;
    int CurrWeaponSlot = 0;

    bool bIsReloading = false;

    FRotator SpineRotator = FRotator(0);

    // Tick Func
    UFUNCTION()
    void MovementTick(float DeltaTime);

    //Func
    UFUNCTION(BlueprintCallable)
    void AttackCharEvent(bool bIsFiring);
    UFUNCTION(BlueprintCallable)
    float GetSpineAngle();
    UFUNCTION(BlueprintCallable)
    void CharacterUpdate();
    UFUNCTION(BlueprintCallable)
    void ChangeMovementState();
    UFUNCTION(BlueprintCallable)
    void Die();
    UFUNCTION(BlueprintCallable)
    void SetRagdoll();

    UFUNCTION(BlueprintCallable)
    bool GetReloadState();
    UFUNCTION(BlueprintCallable)
    void AddWeapon(FName Name, int AmmoInClip);
    UFUNCTION(BlueprintCallable)
	AWeaponDefault* GetCurrentWeapon();
    UFUNCTION(BlueprintCallable)
    void SelectWeapon(int Slot);
    UFUNCTION(BlueprintCallable)
    void TryReloadWeapon();
    UFUNCTION()
    void WeaponFire(UAnimMontage* FireAnim,UAnimMontage* FireAimAnim);
    UFUNCTION()
    void WeaponReloadStart(UAnimMontage* CharAnim);
    UFUNCTION()
    void WeaponReloadEnd(FName AmmoName, int AmmoLoaded);
    UFUNCTION(BlueprintCallable)
    void WeaponDrop();

    UFUNCTION(BlueprintCallable)
    UDecalComponent* GetCursorToWorld();

    UFUNCTION()
    float GetSmoothTargetArmLength(const float AxisValue, float& TargetZoom, float CurrentZoom);
    UFUNCTION()
    void CameraZoom(float Value);
    UFUNCTION()
    void ChangeMovementMode(EMovementState NewMovementState, bool State);
    DECLARE_DELEGATE_TwoParams(FInputStateDelegate, EMovementState, bool);
    DECLARE_DELEGATE_OneParam(FSelectedWeaponDelegate, int);
};
